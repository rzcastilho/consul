[![](https://images.microbadger.com/badges/image/rodrigozc/consul.svg)](https://microbadger.com/images/rodrigozc/consul "Get your own image badge on microbadger.com") [![](https://images.microbadger.com/badges/version/rodrigozc/consul.svg)](https://microbadger.com/images/rodrigozc/consul "Get your own version badge on microbadger.com")

# Consul + Consul Template

## Description

This image starts `Consul` and other processes in folder `/docker/run`. For now, in this folder, scripts have been created just to run `Consul Template`. To customize, just remove processes that's no matters, and/or add others.

### Processes

  1. #### Consul

  The `Consul` process, which is initialized by `/consul.sh` script, uses `dump-init` to run the process, this allows `Consul` process to capture signals from OS like `SIGTERM` and `SIGHUP` to reload configuration or be `gracefully shutdown`.

  2. #### Consul Template

  `Consul Template` watches for updates on `key-pair values` stored by `Consul` to reload templates that use these `key-pair values`. Any update to this information triggers the rewrite of template and, optionally, runs a command that could be to restart a service that uses the template.

  The template configurations was in `/etc/consul-template`, that files contain a `.list` extension. The script `/etc/consul-template/consul-template-loader.sh` read all files inside `/etc/consul-template` folder with `.list` extension, sorting by name, and for each line of these files is created a entry for `Consul template` configuration.

  #### Example

  Record in `.list` file:
  ```
  /etc/filebeat/filebeat.ctmpl:/etc/filebeat/filebeat.yml:/docker/run/filebeat.sh
  ```

  `/etc/consul-template/config.hcl` entry:
  ```
  template {
        source = "/etc/filebeat/filebeat.ctmpl"
        destination = "/etc/filebeat/filebeat.yml"
        command = "/docker/run/filebeat.sh"
  }
  ```

### Locale e Localtime Configuration

By default, this image is configured to use `en_US.UTF-8` locale and `UTC` localtime.

To change this configuration, set the variables `LOCALE` and `LOCALTIME`.

Examples:

##### Dockerfile

```
FROM consul:latest
...
ENV LOCALE "pt_BR.UTF-8"
ENV LOCALTIME "America/Sao_Paulo"
...
```

##### docker-compose.yml

```
version: '3'
services:
  consul-client:
    image: consul:latest
    environment:
      - 'LOCALE=pt_BR.UTF-8'
      - 'LOCALTIME=America/Sao_Paulo'
```

##### Docker command line

```
$ docker run -e 'LOCALE=pt_BR.UTF-8' \
             -e 'LOCALTIME=America/Sao_Paulo' \
             consul:latest
```
